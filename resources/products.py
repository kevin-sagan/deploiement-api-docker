from flask import jsonify
from flask import request
from flask import abort
from flask_restful import Resource
from database.models import Product
import requests
import json

## /products
class AllProducts(Resource):
    def get(self):
        products = Product.objects()
        return jsonify(products)

## /products/<bar_code>
def getProductByCode(bar_code):
    response = requests.get(f"https://world.openfoodfacts.org/api/v0/product/{bar_code}.json")
    info = response.json()
    product = info["product"]
    nutriments = product["nutriments"]

    name = product.get("product_name") or "Not found"
    description = product.get("generic_name_fr") or "Not found"
    barCode = product.get("code")
    caloriesByPortion = nutriments.get("energy-kcal_serving") or 0
    caloriesBy100gr = nutriments.get("energy-kcal_100g") or 0

    return {"name": name,
            "description": description,
            "barCode": barCode,
            "caloriesByPortion": caloriesByPortion,
            "caloriesBy100gr": caloriesBy100gr}

class ProductByBarCode(Resource):
    def get(self, bar_code):
        result = None

        try:
            result = getProductByCode(bar_code)
            product = Product(**result)
            product.save()
            return jsonify(result)

        except Exception as e:
            print(e)
            return {'error': 'User update failed : %s' % e}, 400