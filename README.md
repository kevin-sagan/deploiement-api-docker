# Projet Big Data Architecture

[[_TOC_]]


#### Membres de l'équipe

* Kevin SAGAN
* Fadi EL CHEIKH TAHA
* Roumaissa OMARI
* Julien RIBEIRO
* Marie MENDY
* Celia GUYOBON

## Objectif du projet

Dans le cadre de ce projet, nous avons apporté de nouvelles fonctionnalités à une API que nous avons développée précédemment en cours.
Nous avons donc implémenté l'API de OpenFoodFact dans notre API afin de pouvoir retrouver les informations d'un produit alimentaire à partir de son code-barre.
À l’aide des informations récupérées sur OpenFoodFact, nous pouvons proposer à nos utilisateurs de suivre leur consommation calorique quotidienne.

Dans un premier temps, nous avons étudié la documentation d'OpenFoodFact et leur réponse suite aux requêtes. Ensuite, nous avons récupéré différentes informations tels que le nom du produit, sa description, l'énergie par portion et par produit à partir du code-barre du produit.
Avant d'implémenter cette fonctionnalité dans notre API, nous avons développé cette fonction sur notebook, ce dernier est intitulé "OpenFoodFactAPI.ipynb" sur mon Gitlab.

Afin d'initier les nouveaux modèles dans notre base de données MongoDB, nous avons déclaré le modèle Product, ProductQuantity et ProductIntake avec les différents champs nécessaires pour ces derniers.
Dans la documentation MongoDB Engine, nous pouvons voir les différents types de champs existant, que nous pourrons utiliser pour déclarer les nôtres.

![models](/uploads/2e9cd3f432adb3d197b343495f9d9bb5/image.png)

Pour le modèle Product, nous pouvons voir les champs déclarés. Le champ "name" est un string. Il devra être déclaré comme un StringField.
Les autres champs que nous pouvons constater sur cette capture d'écran sont les IntField, que nous avons utilisés pour les champs à nombre entier, DateTimeField pour les dates et ReferenceField pour lier les différents entre les différents modèles.


## Fonctionnalités ajoutées au projet

Les différentes fonctionnalités ajoutées au projet sont :
- Une route **GET /products/<bar_code>** faisant appel à l'API de OpenFoodFact. À l'aide d'une fonction, elle récupérera et stockera les informations récupérées dans le modèle Product présenté au-dessus.
- Une route **PUT /users/me/goal** permettant de définir l'objectif calorique quotidien de l'utilisateur.
- Une route **PUT /users/manual/product** permettant à l'utilisateur d'ajouter un produit de façon manuelle à la liste des produits qu'il a consommés dans la journée.
- Une route **PUT /users/product/:bar_code** donnant la possibilité à l'utilisateur d'ajouter le produit correspondant au code-barre à la liste de produit qu'il a consommé dans la journée, grâce à OpenFoodFact.
- Une route **GET /users/me/stats/:date** permettant à l'utilisateur de savoir où il se situe par rapport à son objectif calorique quotidien pour une date donnée.

### Déclaration d'une route

Notre code est décomposé en plusieurs fichiers .py, pour faciliter la compréhension du code.

![Déclaration de routes Flask](/uploads/d73f304e4735bd765ef5012fcaa5d2c8/image.png)

Nous constatons dans la capture d'écran, les différentes routes déclarées dans notre API.

api est la variable qui fait appel à notre API.
La méthode add_resource permet de déclarer les différentes routes, elle prend en paramètres la fonction qui sera lancée à l'appel de la route, ainsi que son chemin. Pour la première route présentée, nous voyons le chemin spécifié **/products/<bar_code>**, ce qui signifie qu'une fois le serveur lancé, nous pourrons faire des appels sur cette route soit en passant par un navigateur internet tel que Google Chrome, soit par Postman, qui est un logiciel nous simplifiant les appels.

Nous lançons notre serveur avec la commande 

```python
flask run --port=3000
```

Nous aurons donc accès à notre API sous l'adresse suivante :
```
localhost:3000
```

#### GET /products/<bar_code>

Cette première route permet de récupérer les informations souhaitées et de les stocker dans notre modèle mongoDB Product.

Cette première fonction permet de récupérer ces informations. On y voit en paramètre, qu'un code-barre est attendu.

![getProductByCode fonction](/uploads/d4e2d99a11f59eebfe7fceec10838215/image.png)

La fonction nous permet de récupérer rapidement les informations sans passer par plusieurs liens produits. La seule action à faire sera de changer le code-barre en paramètre, au lieu de changer tout le lien.

Nous récupérons donc la réponse de la requête, puis nous la stockons en json dans la variable info.
Nous pouvons visualiser la réponse de la requête sous forme json

![Redirection vers un produit OpenFoodFacts](/uploads/35d1d791b617139cfec87928497bc82a/image.png)

Un dictionnaire peut contenir plusieurs sous-dictionnaires.

```python
product = info["product"]
nutriments = product["nutriments"]
```

Les informations telles que "name", "description" et "barCode" se trouvent dans le premier dictionnaire "product", tandis que les informations sur les calories se trouvent dans un sous-dictionnaire intitulé "nutriments".

```python
  name = product.get("product_name") or "Not found"
  description = product.get("generic_name_fr") or "Not found"
  barCode = product.get("code")
  caloriesByPortion = nutriments.get("energy-kcal_serving") or 0
  caloriesBy100gr = nutriments.get("energy-kcal_100g") or 0
```

La méthode .get permet de récupérer les informations contenues dans le doctionnaire à l'aide du code-barre saisi.
Pour récupérer le Nom du produit, on chercher si l'information est remplie dans "product_name". Si une information est retrouvée, elle sera retournée. Si l'information n'est pas retrouvée ou qu'il s'agit un champ vide (ce qui arrive dans plusieurs cas avec la description), on nous retournera "Not found".

```python
return {"name": name,
        "description": description,
        "barCode": barCode,
        "caloriesByPortion": caloriesByPortion,
        "caloriesBy100gr": caloriesBy100gr}
```
Enfin, nous retournons l'information de la fonction sous forme de dictionnaire.


Cette route a été définie par la classe ProductByBarCode :

```python
api.add_resource(ProductByBarCode, '/products/<bar_code>')
```

Celle-ci, fait appel à notre fonction getProductByCode, puis sauvegarde les informations récupérées dans la variable result.
Les informations stockées dans result seront par la suite mis en place dans notre modèle Product, puis sauvegardées.

```python
class ProductByBarCode(Resource):
    def get(self, bar_code):
        result = None

        try:
            result = getProductByCode(bar_code)
            product = Product(**result)
            product.save()
            return jsonify(result)

        except Exception as e:
            print(e)
            return {'error': 'User update failed : %s' % e}, 400
```

Sur Postman, nous pouvons vérifier notre route.

![getProduct résultat](/uploads/dcea5a1f137c1344acb71e34d5c40d5e/image.png)

Nous constatons que la route nous renvoie bien les informations comme souhaité.

Afin de vérifier les produits récupérés, nous avons créé une nouvelle route.

```python
class AllProducts(Resource):
    def get(self):
        products = Product.objects()
        return jsonify(products)
```

Dans la variable "products", nous récupérons les objects stockés dans le modèle mongoDB Product.
On peut constater la liste des produits.

![Liste de produits](/uploads/7844f239d86a74d4d8799168a18cc32f/image.png)


#### PUT /users/me/goal

Cette route de définir l'objectif calorique quotidien de l'utilisateur.

```python
## /users/me/goal
class UserMeGoal(Resource):
    @jwt_required()
    def put(self):
        body = request.json
        try:
            user_id = get_jwt_identity()
            user = User.objects.get(id=user_id)
            user.dailyGoal = body["goal"]
            user.save()
            return jsonify(user)
        except Exception as e:
            print(e)
            return {'error': 'Internal error : %s' % e}, 500
```

Il s'agit d'une saisie de nouvelle information, la route est déclarée en PUT.

Nous sécurisons la route selon l'identifiant utilisateur à l'aide de @jwt_required()
Nous définissons donc que les informations récupérées seront stockées dans la variable "body".

La boucle try vérifiera l'identité de l'utilisateur puis stockera celle-ci dans user_id. 
Les informations saisies sous forme de json dans Postman, devront se présenter comme une clé "goal" avec une valeur, comme indiqué dans le modèle User, puis sauvegardé pour l'utilisateur en question.

Pour rappel :
```python
dailyGoal = db.IntField(min_value=0)
```

![Retour objectif calorique](/uploads/e85a747cc68f3ce095b2b429f3e267bb/image.png)

#### PUT /users/manual/product

Cette route permet à notre utilisateur d'ajouter un produit de façon manuelle à la liste des produits qu'il a consommés dans la journée.

```python
## /users/manual/product
def createDailyIntake(productQuantity, date=datetime.now().date()):
    try:
        user_id = get_jwt_identity()
        user = User.objects.get(id=user_id)

        if DailyIntake.objects.filter(userId=user, date=date):
            dailyIntake = DailyIntake.objects.get(userId=user, date=date)
            dailyIntake.products.append(productQuantity)
        else:
            dailyIntake = DailyIntake()
            dailyIntake.userId = user
            dailyIntake.date = date
            dailyIntake.products.append(productQuantity)
        dailyIntake.save()
        return dailyIntake

    except Exception as err:
        print(err)
        return {'error': 'Internal error : %s' % err}, 500
```

```python
if DailyIntake.objects.filter(userId=user, date=date):
   dailyIntake = DailyIntake.objects.get(userId=user, date=date)
   dailyIntake.products.append(productQuantity)
```

Nous avons déclaré une première fonction, qui en premier lieu filtre sur un utilisateur et une date dans le modèle DailyIntake
Pour rappel, DailtyIntake est la liste produit avec UserId et date référencée.

```python
def createDailyIntake(productQuantity, date=datetime.now().date()):
    try:
        ...
        else:
            dailyIntake = DailyIntake()
            dailyIntake.userId = user
            dailyIntake.date = date
            dailyIntake.products.append(productQuantity)
```
Si l'on n'a pas pu filtrer sur un utilisateur et une date, on récupère le modèle précédent, ProductQuantity, puis on lui récupère l'userId de l'utilisateur concerné et on attribue une date. Par défaut, la date sera celle du jour, c'est défini en paramètre de la fonction avec le bout de code ci-dessous :

```python
date=datetime.now().date()
```

```python
class UserManualProduct(Resource):
    @jwt_required()
    def put(self):
        body = request.json
        product = Product()
        productQuantity = ProductQuantity()
        try:
            product.name = body["name"]
            product.description = body["description"]
            product.caloriesByPortion = body["caloriesByPortion"]
            product.caloriesBy100gr = body["caloriesBy100gr"]
            productQuantity.productId = product
            productQuantity.unit = body["unit"]
            productQuantity.quantity = body["amount"]
            product.save()
            if body.get("date") == None:
                return jsonify(createDailyIntake(productQuantity))
            else:
                return jsonify(createDailyIntake(productQuantity,
                                                 datetime.strptime(body.get("date"), '%d-%m-%Y').date()))
```
La suite du code permet de définir le nouveau produit que l'on souhaite ajouter. On aura donc les champs à remplir si on le souhaite, selon l'obligation définie dans la déclaration du modèle.
La condition if, else permet d'ajouter toutes les informations du produit en réutilisant la fonction createDailyIntake précédemment définie selon la présence ou non du champ date.
Si le champ date est rempli, on récupérera les informations telles qu'elles sont fournies. Autrement, la date du jour actuel sera prédéfinie par défaut.


#### PUT /users/product/:bar_code

Cette route donne la possibilité à l'utilisateur d'ajouter le produit correspondant au code-barre à la liste de produit qu'il a consommé dans la journée.

```python
## /users/product/<bar_code>
class UserProductBarcode(Resource):
    @jwt_required()
    def put(self, bar_code):
        body = request.json
        productQuantity = ProductQuantity()

        try:
            productFound = getProductByCode(bar_code)
            productSaved = Product(**productFound)
            productSaved.save()

            productQuantity.productId = productSaved
            productQuantity.unit = body["unit"]
            productQuantity.quantity = body["amount"]

            if body.get("date") == None:
                return jsonify(createDailyIntake(productQuantity))
            else:
                return jsonify(createDailyIntake(productQuantity,
                                                 datetime.strptime(body.get("date"), '%d/%m/%Y').date()))

        except AttributeError as e:
            return {'error': 'Product not found. Check the barcode. %s' % e}, 404

        except Exception as e:
            return {'error': 'Internal error : %s' % e}, 500
```
```python
            productFound = getProductByCode(bar_code)
            productSaved = Product(**productFound)
            productSaved.save()

            productQuantity.productId = productSaved
            productQuantity.unit = body["unit"]
            productQuantity.quantity = body["amount"]
```

Si nous trouvons le produit, nous le stockons dans les champs de Product, puis nous le sauvegardons.

Les nouveaux champs à remplir ici sont "unit" et "amount".
- "unit" étant l'unité de mesure de la quantité de produit, donc soit par "portion" soit par "gram"
- "amount" étant un nombre entier positif qui représente la quantité de produit consommé.

Voici le retour de la requête sur Postman.

![Retour Postman quantity & unit](/uploads/12a4e35eac77be94533d93255874f1d9/image.png)

#### GET /users/me/stats/:date

Cette route permet à notre utilisateur de savoir où il se situe par rapport à son objectif calorique quotidien pour la date donnée.

```python
class UserGoalDate(Resource):
    @jwt_required()
    def get(self, date):
        try:
            user_id = get_jwt_identity()
            user = User.objects.get(id=user_id)
            date = datetime.strptime(date, '%d-%m-%Y').date()

            if DailyIntake.objects.filter(userId=user, date=date):
                dailyIntake = DailyIntake.objects.get(userId=user, date=date)

        except Exception as e:
            print(e)
            return {'error': 'Internal error : %s' % e}, 500

        for productQuantity in dailyIntake.products:
            if productQuantity.unit == "portion":
                calories = productQuantity.quantity * productQuantity.productId.caloriesByPortion
            else:
                calories = productQuantity.quantity * productQuantity.productId.caloriesBy100gr / 100

        return {"daily_goal": user.dailyGoal,
                "daily_intake": calories,
                "daily_result": int(calories) - int(user.dailyGoal)}
```

Dans la fonction try, nous récupérons les informations de l'utilisateur ainsi que la date qu'on déclare sous la forme suivante :
```python
date = datetime.strptime(date, '%d-%m-%Y').date()
```
Nous souhaitons simplement le jour, le mois et l'année, donc nous rajoutons la méthode .date(), autrement, nous récupérions aussi les heures, minutes et secondes.

```python
  if DailyIntake.objects.filter(userId=user, date=date):
     dailyIntake = DailyIntake.objects.get(userId=user, date=date)

except Exception as e:
    print(e)
    return {'error': 'Internal error : %s' % e}, 500
```
Si nous retrouvons les informations de l'utilisateur, nous lui renverrons, sinon, nous renvoyons une erreur.

```python
        for productQuantity in dailyIntake.products:
            if productQuantity.unit == "portion":
                calories = productQuantity.quantity * productQuantity.productId.caloriesByPortion
            else:
                calories = productQuantity.quantity * productQuantity.productId.caloriesBy100gr / 100

        return {"daily_goal": user.dailyGoal,
                "daily_intake": calories,
                "daily_result": int(calories) - int(user.dailyGoal)}
```

Si nous retrouvons la consommation par portion, nous retournons les informations dans la variable calories.
S'il ne s'agit pas de portion mais de calories par 100 grammes, nous faisons le calcul puis le stockons dans la variable calories.

Nous retournons enfin les informations.

![Retour statistique du jour](/uploads/f662c89b6eb706277f99ab08d247716b/image.png)
