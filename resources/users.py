from flask import jsonify
from flask import request
from flask import abort
from flask_restful import Resource
from database.models import User, Product, ProductQuantity, DailyIntake
from resources.products import getProductByCode
from flask_jwt_extended import jwt_required, get_jwt_identity
from datetime import datetime

## /users routes
class UsersApi(Resource):
    def get(self):
        users = User.objects()
        return jsonify(users)

    def delete(self):
        users = User.objects().delete()
        return jsonify({message: "all users deleted" % userId})

## /users/<userId> routes
class UserApi(Resource):
    def get(self, userId):
        try:
            user = User.objects.get(id=userId)
            return jsonify(user)
        except Exception as e:
            print(e)
            return {'error': 'User not found : %s' % e}, 404
    
    def put(self, userId):
        body = request.json
        try:
            User.objects.get(id=userId).update(**body)
            return jsonify(User.objects.get(id=userId))
        except Exception as e:
            print(e)
            return {'error': 'User update failed : %s' % e}, 400  

    def delete(self, userId):
        try:
            User.objects.get(id=userId).delete()
            return jsonify({message: "user %s deleted" % userId})
        except Exception as e:
            print(e)
            return {'error': 'Couldnt delete user : %s' % e}, 400

## /users/me
class UserMeApi(Resource):
    @jwt_required()
    def get(self):
        try:
            # extract info from the jwt token
            user_id = get_jwt_identity()
            user = User.objects.get(id=user_id)
            return jsonify(user)
        except Exception as e:
            print(e)
            return {'error': 'Internal error : %s' % e}, 500

## /users/me/goal
class UserMeGoal(Resource):
    @jwt_required()
    def put(self):
        body = request.json
        try:
            user_id = get_jwt_identity()
            user = User.objects.get(id=user_id)
            user.dailyGoal = body["goal"]
            user.save()
            return jsonify(user)
        except Exception as e:
            print(e)
            return {'error': 'Internal error : %s' % e}, 500

## /users/manual/product
def createDailyIntake(productQuantity, date=datetime.now().date()):
    try:
        user_id = get_jwt_identity()
        user = User.objects.get(id=user_id)

        if DailyIntake.objects.filter(userId=user, date=date):
            dailyIntake = DailyIntake.objects.get(userId=user, date=date)
            dailyIntake.products.append(productQuantity)
        else:
            dailyIntake = DailyIntake()
            dailyIntake.userId = user
            dailyIntake.date = date
            dailyIntake.products.append(productQuantity)
        dailyIntake.save()
        return dailyIntake

    except Exception as err:
        print(err)
        return {'error': 'Internal error : %s' % err}, 500


class UserManualProduct(Resource):
    @jwt_required()
    def put(self):
        body = request.json
        product = Product()
        productQuantity = ProductQuantity()

        try:
            product.name = body["name"]
            product.description = body["description"]
            product.caloriesByPortion = body["caloriesByPortion"]
            product.caloriesBy100gr = body["caloriesBy100gr"]
            productQuantity.productId = product
            productQuantity.unit = body["unit"]
            productQuantity.quantity = body["amount"]
            product.save()

            if body.get("date") == None:
                return jsonify(createDailyIntake(productQuantity))
            else:
                return jsonify(createDailyIntake(productQuantity,
                                                 datetime.strptime(body.get("date"), '%d-%m-%Y').date()))

        except Exception as e:
            print(e)
            return {'error': 'Internal error : %s' % e}, 500


## /users/product/<bar_code>
class UserProductBarcode(Resource):
    @jwt_required()
    def put(self, bar_code):
        body = request.json
        productQuantity = ProductQuantity()

        try:
            productFound = getProductByCode(bar_code)
            productSaved = Product(**productFound)
            productSaved.save()

            productQuantity.productId = productSaved
            productQuantity.unit = body["unit"]
            productQuantity.quantity = body["amount"]

            if body.get("date") == None:
                return jsonify(createDailyIntake(productQuantity))
            else:
                return jsonify(createDailyIntake(productQuantity,
                                                 datetime.strptime(body.get("date"), '%d/%m/%Y').date()))

        except AttributeError as e:
            return {'error': 'Product not found. Check the barcode. %s' % e}, 404

        except Exception as e:
            return {'error': 'Internal error : %s' % e}, 500


## /users/me/stats/<date>
class UserGoalDate(Resource):
    @jwt_required()
    def get(self, date):
        try:
            user_id = get_jwt_identity()
            user = User.objects.get(id=user_id)
            date = datetime.strptime(date, '%d-%m-%Y').date()

            if DailyIntake.objects.filter(userId=user, date=date):
                dailyIntake = DailyIntake.objects.get(userId=user, date=date)

        except Exception as e:
            print(e)
            return {'error': 'Internal error : %s' % e}, 500

        for productQuantity in dailyIntake.products:
            if productQuantity.unit == "portion":
                calories = productQuantity.quantity * productQuantity.productId.caloriesByPortion
            else:
                calories = productQuantity.quantity * productQuantity.productId.caloriesBy100gr / 100

        return {"daily_goal": user.dailyGoal,
                "daily_intake": calories,
                "daily_result": int(calories) - int(user.dailyGoal)}



## /users/recipes
class UserRecipesApi(Resource):
    @jwt_required()
    def put(self):
        body = request.json
        try:
            # extract info from the jwt token
            user_id = get_jwt_identity()
            user = User.objects.get(id=user_id)
            for r in body["recipes"]:
                ## appeller strapi pour récupérer la recette
                user.recipes.append(r)
            user.save()
            return jsonify(user)
        except Exception as e:
            print(e)
            return {'error': 'Internal error : %s' % e}, 500