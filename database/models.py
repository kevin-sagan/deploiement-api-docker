from .db import db
from flask_bcrypt import generate_password_hash, check_password_hash

class User(db.Document):
 email = db.EmailField(required=True, unique=True)
 password = db.StringField(required=True, min_length=6)
 firstName = db.StringField()
 lastName = db.StringField()
 recipes = db.ListField(db.IntField())
 dailyGoal = db.IntField(min_value=0)

 def hash_password(self):
  self.password = generate_password_hash(self.password).decode('utf8')

 def check_password(self, password):
  return check_password_hash(self.password, password)

class Product(db.Document):
 name = db.StringField(required=True)
 description = db.StringField()
 barCode = db.StringField(unique=True, sparse=True)
 caloriesByPortion = db.IntField(min_value=0, required=True)
 caloriesBy100gr = db.IntField(min_value=0, required=True)

class ProductQuantity(db.EmbeddedDocument):
 productId = db.ReferenceField(Product)
 unit = db.StringField(choices=("portion", "gram"))
 quantity = db.IntField(min_value=0)

class DailyIntake(db.Document):
 userId = db.ReferenceField(User)
 date = db.DateTimeField()
 products = db.ListField(db.EmbeddedDocumentField(ProductQuantity))